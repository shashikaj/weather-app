class HttpStatusCodes {
    static _404NotFound = 404;
    static _401Unauthorized = 401;
    static _400BadRequest = 400;
}

export default HttpStatusCodes;
