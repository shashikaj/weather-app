class Converter {

    static kelvinToCelcius(kelvin: number) {
        return Math.round(kelvin - 273.15);
    }

    static getTimePart(dateTime: string) {
        let parts = dateTime.split(' ');
        return parts[1];
    }

}

export default Converter;