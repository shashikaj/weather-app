import axios from 'axios';

const instance = axios.create({
    baseURL: 'http://api.openweathermap.org',
});

instance.interceptors.request.use(
    config => {

        config.params = {
            ...config.params,
            appid: "2fe4b982ae1964400d792e6ae21a37fd"
        }

        return config
    },
    error => {
        console.log(error);
    }
);

export default instance;