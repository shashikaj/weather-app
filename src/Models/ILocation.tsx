export default interface ILocation {
    name: string,
    country: string
}