import React from 'react';
import Converter from '../../Helpers/Converter';
import './DailyForecastCard.scss';
import '../../Helpers/Converter';
import ILocation from '../../Models/ILocation';
import IWeatherForecast from '../../Models/IWeatherForecast';

interface IProps {
    city: ILocation,
    data: IWeatherForecast[]
}

function dailyForecastCard(props: IProps) {

    return(
        <div className="col-sm-12 col-md-12 col-lg-6 col-xl-6 border mt-3 p-3 weather-card">
            <h4><strong>{props.city.name}, {props.city.country}</strong></h4>

            <img 
                src={"http://openweathermap.org/img/wn/iconId@2x.png".replace('iconId', props.data[0].weather[0].icon)} 
                className="icon"
                alt="icon" />

            <span className="temp">{Converter.kelvinToCelcius(props.data[0].main.temp)}°C</span>

            <div className="row px-2">
                <span className="col-12 label">Feels Like: 
                    {' ' + Converter.kelvinToCelcius(props.data[0].main.feels_like)}°C, 
                    {' ' + props.data[0].weather[0].main}, 
                    {' ' + props.data[0].weather[0].description}
                </span>

                {/* <span className="col-6 title">Humidity: <span className="col-6 label">{props.data[0].main.humidity}%</span></span> */}

                <table className="text-center mt-2">    
                    
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Temp</th>
                            <th>Humidity</th>
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>{Converter.getTimePart(props.data[1].dt_txt)}</td>
                            <td><img
                                alt="icon" 
                                width="30" 
                                height="30" 
                                src={"http://openweathermap.org/img/wn/iconId@2x.png".replace('iconId', props.data[1].weather[0].icon)} 
                                className="icon" /></td>
                            <td>{props.data[1].weather[0].description}</td>
                            <td>{Converter.kelvinToCelcius(props.data[1].main.temp)}°C</td>
                            <td>{props.data[1].main.humidity}%</td>
                        </tr>

                        <tr>
                            <td>{Converter.getTimePart(props.data[2].dt_txt)}</td>
                            <td><img
                                alt="icon" 
                                width="30" 
                                height="30" 
                                src={"http://openweathermap.org/img/wn/iconId@2x.png".replace('iconId', props.data[2].weather[0].icon)} 
                                className="icon" /></td>
                            <td>{props.data[2].weather[0].description}</td>
                            <td>{Converter.kelvinToCelcius(props.data[2].main.temp)}°C</td>
                            <td>{props.data[2].main.humidity}%</td>
                        </tr>

                        <tr>
                            <td>{Converter.getTimePart(props.data[3].dt_txt)}</td>
                            <td><img
                                alt="icon" 
                                width="30" 
                                height="30" 
                                src={"http://openweathermap.org/img/wn/iconId@2x.png".replace('iconId', props.data[3].weather[0].icon)} 
                                className="icon" /></td>
                            <td>{props.data[3].weather[0].description}</td>
                            <td>{Converter.kelvinToCelcius(props.data[3].main.temp)}°C</td>
                            <td>{props.data[3].main.humidity}%</td>
                        </tr>

                        <tr>
                            <td>{Converter.getTimePart(props.data[4].dt_txt)}</td>
                            <td><img
                                alt="icon" 
                                width="30" 
                                height="30" 
                                src={"http://openweathermap.org/img/wn/iconId@2x.png".replace('iconId', props.data[4].weather[0].icon)} 
                                className="icon" /></td>
                            <td>{props.data[4].weather[0].description}</td>
                            <td>{Converter.kelvinToCelcius(props.data[4].main.temp)}°C</td>
                            <td>{props.data[4].main.humidity}%</td>
                        </tr>                                        
                    </tbody>

                </table>
            </div>

        </div>
    );
}

export default dailyForecastCard;