import React from 'react';
import logo from '../../logo.svg';

function navigation() {
    return(
        <nav className="navbar navbar-expand-lg navbar-light bg-light border-bottom">
            <div className="container-fluid">
                <div className="navbar-brand">
                    <img className="mr-3" src={logo} width="30" height="30" alt="" loading="lazy" />
                    <strong>Weather App</strong>
                </div>
            </div>
        </nav>
    );
}

export default navigation;