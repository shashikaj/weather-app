import React from 'react';
import Slider from 'react-slick';
import WeatherCard from './WeatherCard/WeatherCard';

interface IProps {
    data: any
}

function Carousel(props: IProps) {

    var settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 3,
        slidesToScroll: 1,
        responsive: [
            {
              breakpoint: 1200,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                infinite: true,
                dots: true
              }
            },
            {
              breakpoint: 767,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                initialSlide: 1
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
        ]        
    };

    return(
        <Slider {...settings}>
            <WeatherCard data={props.data.slice(0, 8)} />
            <WeatherCard data={props.data.slice(8, 16)} />
            <WeatherCard data={props.data.slice(16, 24)} />
            <WeatherCard data={props.data.slice(24, 32)} />
            <WeatherCard data={props.data.slice(32, 40)} />
        </Slider>
    );
}

export default Carousel;