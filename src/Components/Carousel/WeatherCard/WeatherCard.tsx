import React from 'react';
import Converter from '../../../Helpers/Converter';
import './WeatherCard.scss';

interface IProps {
    data: any
}

function WeatherCard(props: IProps) {

    return(
        <div className="col-12 text-center carousel-card">
            <img 
                src={"http://openweathermap.org/img/wn/iconId@2x.png".replace('iconId', '10d')} 
                className="icon mx-auto" 
                alt="icon" />
            <h4 className="temp">{Converter.kelvinToCelcius(props.data[0].main.temp)}°C</h4>

            <div className="row p-2">
                <span className="col-6 title">Feels Like: <span className="col-6 label">{Converter.kelvinToCelcius(props.data[0].main.feels_like)}°C</span></span>
                <span className="col-6 title">Humidity: <span className="col-6 label">{props.data[0].main.humidity}%</span></span>

                <table className="mt-2">    
                    
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th>Temp</th>
                            {/* <th>Humidity</th> */}
                        </tr>
                    </thead>

                    <tbody>
                        <tr>
                            <td>{Converter.getTimePart(props.data[1].dt_txt)}</td>
                            <td><img 
                                alt="icon"
                                width="30" 
                                height="30" 
                                src={"http://openweathermap.org/img/wn/iconId@2x.png".replace('iconId', props.data[1].weather[0].icon)} 
                                className="icon" /></td>
                            <td>{props.data[1].weather[0].description}</td>
                            <td>{Converter.kelvinToCelcius(props.data[1].main.temp)}°C</td>
                            {/* <td>{props.data[1].main.humidity}%</td> */}
                        </tr>

                        <tr>
                            <td>{Converter.getTimePart(props.data[2].dt_txt)}</td>
                            <td><img 
                                alt="icon"
                                width="30" 
                                height="30" 
                                src={"http://openweathermap.org/img/wn/iconId@2x.png".replace('iconId', props.data[2].weather[0].icon)} 
                                className="icon" /></td>
                            <td>{props.data[2].weather[0].description}</td>
                            <td>{Converter.kelvinToCelcius(props.data[2].main.temp)}°C</td>
                            {/* <td>{props.data[2].main.humidity}%</td> */}
                        </tr>

                        <tr>
                            <td>{Converter.getTimePart(props.data[3].dt_txt)}</td>
                            <td><img 
                                alt="icon"
                                width="30" 
                                height="30" 
                                src={"http://openweathermap.org/img/wn/iconId@2x.png".replace('iconId', props.data[3].weather[0].icon)} 
                                className="icon" /></td>
                            <td>{props.data[3].weather[0].description}</td>
                            <td>{Converter.kelvinToCelcius(props.data[3].main.temp)}°C</td>
                            {/* <td>{props.data[3].main.humidity}%</td> */}
                        </tr>

                        <tr>
                            <td>{Converter.getTimePart(props.data[4].dt_txt)}</td>
                            <td><img 
                                alt="icon"
                                width="30" 
                                height="30" 
                                src={"http://openweathermap.org/img/wn/iconId@2x.png".replace('iconId', props.data[4].weather[0].icon)} 
                                className="icon" /></td>
                            <td>{props.data[4].weather[0].description}</td>
                            <td>{Converter.kelvinToCelcius(props.data[4].main.temp)}°C</td>
                            {/* <td>{props.data[4].main.humidity}%</td> */}
                        </tr>                                        
                    </tbody>

                </table>
            </div>

        </div>

    );
    
}

export default WeatherCard;