import React, { useState } from 'react';
import Navigation from '../Navigation/Navigation';
import Input from '../UI/Input/Input';
import Axios from '../../axios';
import ILocation from '../../Models/ILocation';
import IWeatherForecast from '../../Models/IWeatherForecast';
import DailyForecastCard from '../DailyForecastCard/DailyForecastCard';
import Carousel from '../Carousel/Carousel';
import './Home.scss';
import HttpStatusCodes from '../../Helpers/HttpStatusCodes';
import Notification from '../Notification/Notification';

function Home() {

    let [cityInput, setCityInput] = useState({
        configs: {
            name: "city",
            inputType: "text",
        },
        value: ""
    });

    let [location, setLocation] = useState<ILocation>({name: "", country: ""});
    let [weatherForecast, setWeatherForecast] = useState<IWeatherForecast[]>([]);

    const inputChangeHandler = (event: any) => {
        if (event.key === 'Enter') {
            fetchWeatherForecasts();
            return;
        }

        setCityInput({
            ...cityInput,
            value: event.target.value
        });
    }

    function fetchWeatherForecasts() {
        Axios.get('/data/2.5/forecast', {params: {q: cityInput.value}})
            .then(response => {
                setCityInput({
                    ...cityInput,
                    value: ""
                });

                let loc = {name: response.data.city.name, country: response.data.city.country};
                setLocation({
                    ...location,
                    name: loc.name,
                    country: loc.country
                })

                let data: [] = response.data.list;
                setWeatherForecast(data);
            })
            .catch(error => {
                switch (error.response.status) {
                    case HttpStatusCodes._404NotFound:
                        Notification.showErrorToast(error.response.data.message);
                        break;
                    case HttpStatusCodes._401Unauthorized:
                        Notification.showErrorToast(error.response.data.message);
                        break;
                    case HttpStatusCodes._400BadRequest:
                        Notification.showErrorToast(error.response.data.message);
                        break;
                    default:
                        Notification.showErrorToast("Something went wrong. Please try again.");
                        break;
                }
                
            });
    }

    return(
        <div>
            <Navigation />
            <div className="container">
                <div className="row justify-content-center">
                    <div className="input-group mt-3 col-sm-12 col-lg-5 col-xl-5 mb-3">
                        <Input changed={inputChangeHandler} value={cityInput.value} configs={cityInput.configs} />
                        <button className="btn btn-outline-secondary" type="button" onClick={fetchWeatherForecasts}>Search</button>
                    </div>

                    {/* today's weather */}
                    {
                        weatherForecast.length !== 0 
                        ? <DailyForecastCard data={weatherForecast.slice(0, 8)} city={location} />
                        : null
                    }

                    {/* carousel */}
                    {   
                        weatherForecast.length !== 0 
                        ? <div className="col-12 my-5"><Carousel data={weatherForecast} /></div>
                        : null
                    }
                    
                </div>
            </div>
        </div>
    );
}

export default Home;