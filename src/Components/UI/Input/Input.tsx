import React from 'react';

interface IProps {
    changed: (event: any) => void,
    configs: {
        name: string,
        inputType: string
    },
    value: string,
}

function Input(props: IProps) {

    function handleKeyDown (event: any) {
        if (event.key === 'Enter') {
            props.changed(event);
        }
    }

    switch (props.configs.inputType) {
        case "text":
            return (
                    <input 
                        type="text" 
                        name={props.configs.name}
                        className="form-control" 
                        placeholder="Enter your city"
                        onKeyDown={handleKeyDown}
                        onChange={props.changed} />

            );
        default:
            return <input />
    }

}

export default Input;