import { toast, ToastOptions } from 'react-toastify';

class Notification {

    private static toastOptions: ToastOptions = {
        position: "bottom-right",
        autoClose: 5000,
        hideProgressBar: true,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: false,
        progress: undefined,
    };

    static showErrorToast(message: string) {
        toast.error("Oops! " + message, Notification.toastOptions);
    }

    // static showWarningToast(message: string) {
    //     toast.warn("Oops! " + message, Notification.toastOptions);
    // }

}

export default Notification;