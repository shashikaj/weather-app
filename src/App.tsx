import React from 'react';
import './App.scss';
import Home from './Components/Home/Home';
import { ToastContainer } from 'react-toastify';

function App() {
  return (
    <div className="App">
      <Home />
      
      <ToastContainer
        position="bottom-right"
        autoClose={5000}
        hideProgressBar
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable={false}
        pauseOnHover />
    </div>
  );
}

export default App;
